let allWeatherData

async function getDataFromApi(sortType) {
    let response = await fetch('http://localhost:8000/getData', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json; charset=utf-8'
        },
    })
      
    let result = await response.json()

    if (result.error) {
        console.log(result.error)

    } else {
        await sortData(result, sortType)
    }
}

async function sendData(weather) {
    let response = await fetch('http://localhost:8000/create', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json; charset=utf-8'
        },

        body: JSON.stringify(weather)
    })
      
    let result = await response.json()

    if (!result.success) {
        console.log(result.error)

    } else {
        await getDataFromApi()
    }
}

async function removeData(id) {
    let response = await fetch('http://localhost:8000/remove', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json; charset=utf-8'
        },

        body: JSON.stringify({id: id})
    })
      
    let result = await response.json()

    if (!result.success) {
        console.log(result.error)

    } else {
        await getDataFromApi()
    }
}

async function getNewData() {
    let weather = {}

    weather.date = document.getElementById('date').value
    weather.time = document.getElementById('time').value
    weather.temp = document.getElementById('temp').value
    weather.wet = document.getElementById('wet').value
    weather.wind = document.getElementById('wind').value
    weather.type = document.getElementById('type').value

    let validData = await checkData(weather)
    if (validData.checkStatus.err) {
        showMessage(validData.checkStatus.text)

    } else {
        await sendData(validData)
        closeMessage()
    }
}

async function checkData(dataWeather) {
    for (let key in dataWeather) {
        if (dataWeather[key] == '') {
            dataWeather.checkStatus = {err: true, text: 'Все поля должны быть заполнены'} 
            return dataWeather
        }
    }

    +dataWeather.temp
    +dataWeather.wet
    +dataWeather.wind

    if (typeof(dataWeather.temp) == 'NaN' || typeof(dataWeather.wet) == 'NaN' || typeof(dataWeather.wind) == 'NaN') {
        dataWeather.checkStatus = {err: true, text: 'Поля: "Температура", "Влажность", "Скорость ветра" должны содержать число!'} 
        return dataWeather
    }

    dataWeather.checkStatus = {err: false}
    return dataWeather
}

function showMessage(text) {
    let messageBox = document.getElementById('message-box')
    let message = document.getElementById('message')

    messageBox.style.display = 'block'
    message.textContent = text
}

function closeMessage() {
    let messageBox = document.getElementById('message-box')
    messageBox.style.display = 'none'
}

let sortCounter = {
    item: '',
    count: 0
}

function sortData(data, sortType) {
    if (sortType != sortCounter.item) {
        sortCounter.item = sortType
        sortCounter.count = 1

    } else {
        sortCounter.count++
        if (sortCounter.count > 1) sortCounter.count = 0
    }

    data.sort((a, b) => {
        itemA = sortType == 'date' && a.date || sortType == 'time' && a.time || sortType == 'temp' && a.temp || sortType == 'wet' && a.wet || sortType == 'wind' && a.wind || sortType == 'type' && a.type || false
        itemB = sortType == 'date' && b.date || sortType == 'time' && b.time || sortType == 'temp' && b.temp || sortType == 'wet' && b.wet || sortType == 'wind' && b.wind || sortType == 'type' && b.type || false

        if (!itemA && !itemB) {
            if (a.date < b.date) return 1
            if (a.date > b.date) return -1
            return 0

        } else if (sortCounter.count == 0) {
            if (itemA < itemB) return 1
            if (itemA > itemB) return -1
            return 0

        } else {
            if (itemA < itemB) return -1
            if (itemA > itemB) return 1
            return 0
        }
    })

    addNewDataTable(data)
}

async function addNewDataTable(data) {
    document.querySelector('#weatherTable tbody').innerHTML = data.map(item =>
    `<tr>
        <td>${item.date}</td>
        <td>${item.time}</td>
        <td>${item.temp}</td>
        <td>${item.wet}</td>
        <td>${item.wind}</td>
        <td>${item.type}</td>
        <td>
            <div onclick="removeData('${item._id}')">
                <i class="far fa-trash-alt red-icon" title="Удалить"></i>
            </div>
        </td>
    </tr>`).join('')

    allWeatherData = data
    setChart('wind')
}

function exportCsv() {
    weatherData = allWeatherData
    weatherData.map(data => {
        delete data.checkStatus
        delete data._id
    })

    let csvData = objToCsv(allWeatherData)
    downloadData(csvData)
}

function objToCsv(data) {
    let csvRows = []

    let headers = Object.keys(data[0])
    console.log(headers)
    csvRows.push(headers.join(','))

    for (let row of data) {
        let values = headers.map(header => {
            let escaped = (''+row[header]).replace(/"/g, '\\"')
            return `${escaped}`
        })

        csvRows.push(values.join(','))
    }
    csvRows[0] = ['Дата', 'Время', 'Температура' , 'Влажность', 'Скорость ветра', 'Осадки']
    return csvRows.join('\n')
}

function downloadData(data) {
    let blob = new Blob(["\ufeff", data], {type: 'text/csv'})
    let url = window.URL.createObjectURL(blob)
    let a = document.createElement('a')

    a.setAttribute('hidden', '')
    a.setAttribute('href', url)
    a.setAttribute('download', 'weather.csv')
    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
}

function setChart(type) {
    let data = allWeatherData
    let canvas = type == 'wind' && document.getElementById('canvas-wind') || type == 'temp' && document.getElementById('canvas-temp') || type == 'wet' && document.getElementById('canvas-wet')

    let ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, 500, 500);
	
    ctx.fillStyle = "black"
    ctx.lineWidth = 2.0
    ctx.beginPath()
    ctx.moveTo(30, 10)
    if (type == 'temp') {
        ctx.lineTo(30, 220)
        ctx.lineTo(680, 220)
    } else {
        ctx.lineTo(30, 460)
        ctx.lineTo(680, 460)
    }
    ctx.stroke()


    if (type == 'temp') {
        ctx.fillStyle = "black"
        ctx.moveTo(30, 10)
        ctx.lineTo(30, 460)
        ctx.lineTo(680, 460)
        ctx.stroke()

        for (let i = 0; i < 10; i++) { 
            ctx.fillText((30 - 10 * i) + "", 4, i * 80 + 60)
            ctx.beginPath()
            ctx.moveTo(25, i * 40 + 60)
            ctx.lineTo(680, i * 40 + 60)
            ctx.stroke()
        }

    } else {
        for (let i = 0; i < 6; i++) { 
            ctx.fillText((5 - i) * 5 + "", 4, i * 80 + 60)
            ctx.beginPath()
            ctx.moveTo(25, i * 80 + 60)
            ctx.lineTo(30, i * 80 + 60)
            ctx.stroke()
        } 
    }

    let labels = []
    let items = []

    for (let item of data) {
        labels.push(item.date)

        if (type == 'wind') items.push(item.wind)
        if (type == 'wet') items.push(item.wet)
        if (type == 'temp') items.push(item.temp)
    }

    for(let i = 0; i < labels.length; i++) { 
        ctx.fillText(labels[i], 40 + i * 100, 475)
    }

    ctx.fillStyle = '#0B61A4'


    for(let i = 0; i < items.length; i++) { 
        let dp = items[i]
        if (type == 'temp') {
            ctx.fillRect(40 + i * 100, 300 - dp * 8 , 50, dp * 8)

        } else {
            ctx.fillRect(40 + i * 100, 460 - dp * 16 , 50, dp * 16)
        }
    }
}

function selectChart(type) {
    let wind = document.getElementById('g-wind')
    let temp = document.getElementById('g-temp')
    let wet = document.getElementById('g-wet')

    if (type == 'wind') {
        wind.style.display = "block"
        temp.style.display = "none"
        wet.style.display = "none"
        setChart(type)

    } else if (type == 'wet') {
        wind.style.display = "none"
        temp.style.display = "none"
        wet.style.display = "block"
        setChart(type)

    } else if (type == 'temp') {
        wind.style.display = "none"
        temp.style.display = "block"
        wet.style.display = "none"
        setChart(type)
    }
}

getDataFromApi()